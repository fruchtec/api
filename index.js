
let express = require('express');
const axios = require('axios')
const fetch = require('node-fetch')
let cors = require('cors')
require('dotenv').config()

const app = express();
app.use(express.json());
app.use(cors ({
    origin: 'http://localhost:3000'
}))

app.get('/api/graphql', (req, res) => {
    fetch(process.env.SHOPIFY_BASE_URL + '/admin/api/2021-04/graphql.json', {
        method: 'POST',
        headers: {
            'X-Shopify-Access-Token' : process.env.SHOPIFY_API_SECRET,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({query: `
            {
              products(first: 10) {
                edges {
                  node {
                    id
                    bodyHtml
                    title
                    totalInventory
                  }
                }
              }
          }
        `})
    })
      .then(r => r.json())
      .then(data => res.send(data));
});

app.post('/api/createProduct', (req, res) => {
    console.log(req.body)
    fetch(process.env.SHOPIFY_BASE_URL + '/admin/api/2021-04/graphql.json', {
        method: 'POST',
        headers: {
            'X-Shopify-Access-Token' : process.env.SHOPIFY_API_SECRET,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          query: `
          mutation productCreate($input: ProductInput!) {
            productCreate(input: $input) {
              product {
                id
              }
              shop {
                id
              }
              userErrors {
                field
                message
              }
            }
          }
        `,
        variables: {
          input: req.body
        },
        })
    })
      .then(r => r.json())
      .then(data => {
          console.log(data);
          return res.send(data)
      });
});

app.listen(2021, () => {
    console.log("server is running");
});
